# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

alias 0='cd ~/Documents/COSC260/'
alias online='python3 -m http.server'
alias 00='cd ~/Documents/COSC260/em/ence260-ucfk4/labs'

alias 4='cd ~/Documents/COSC264/'
alias 5='cd ~/Documents/SENG365/'
alias well='cd ~ ; cd Documents ; git pull'
alias vim='nvim'
alias uni='ssh -Y htc36@cssecs3.canterbury.ac.nz'
alias uni2='ssh -X htc36@cssecs3.canterbury.ac.nz'
alias vrc='vim ~/.vimrc'
alias brc='vim ~/.bashrc'
alias src='source ~/.bashrc'
alias wow="vim -S ~/session.vim -c 'source ~/.vimrc'"
alias wifi="sudo systemctl restart NetworkManager"
alias con="vim ~/.config/i3/config"
alias ref="i3-msg reload"
alias pol="vim ~/.config/polybar/config"
alias vifm="/home/cosc/student/htc36/.local/bin/bin/vifm"
alias installed="pacman -Qe"
alias connect="nm-applet"
alias server="ssh root@45.76.124.20"
alias db="ssh htc36@linux.cosc.canterbury.ac.nz -L 3306:db2.csse.canterbury.ac.nz:3306 -N"
alias sshR="ssh root@45.76.124.20 -R 4941:localhost:4941"
alias checkTunnel="nmap -p 4941 45.76.124.20"
alias term="vim ~/.config/termite/config"
alias rest='sudo nmcli networking off ; sudo nmcli networking on'
alias toolbox='/opt/jetbrains-toolbox-1.16.6319/jetbrains-toolbox'
alias setclip="xclip -selection c"
alias getclip="xclip -selection c -o"
alias sql="ssh -L localhost:2000:localhost:3306 root@45.76.124.20"
alias addSshServer="ssh-copy-id -i ~/.ssh/id_rsa.pub root@45.76.124.20"

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

alias gotime='git add . ; git commit -m "lll" ; git push'

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    #PS1='lambda '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'


# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [ -f ~/.bashfunctions ]; then
    . ~/.bashfunctions
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

set -o vi
#export PATH=$PATH:/users/harry/bin
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
export PATH=$PATH:$HOME/.local/bin/
export PATH=$PATH:$HOME/bin
export PATH=$PATH:/snap/bin
export EDITOR=nvim
export TERM=xterm-256color
export PATH=$PATH:$HOME/.local/bin/
export PATH=$PATH:$HOME/.local/bin/
export PATH=$PATH:/usr/local/bin

