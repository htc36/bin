
nnoremap <F12> :execute '! astyle --style=1tbs --indent-labels < % > %.temp && mv %.temp %' \| <CR>:e<CR><CR>
map <F1> :w <CR>:terminal source ~/.bashfunctions && c %<CR>
map <F2> :w <CR>:terminal source ~/.bashfunctions && cVal %<CR>
