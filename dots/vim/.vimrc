:set number relativenumber
:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END
cabbrev vb vert belowright sb
cabbrev hb sb
cabbrev v vs
cabbrev h sp 
cabbrev Wq wq 
cabbrev WQ wq 
iabbrev *** *****************************************************************************************************************************

cabbrev qq %bw

map <F5> :w <CR> :source ~/.vimrc <CR>
"map <F2> :w <CR> :terminal python3 % <CR>
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-l> <C-w>l
map <C-k> <C-w>k
map <C-y> :w <CR>:terminal source ~/.bashfunctions && c %<CR>


map zl <C-w>H
map zj <C-w>J
map zk <C-w>K
map zh <C-w>L

map <F6> :vs ~/.vimrc <CR>
map <F3> :vs ~/.bashrc <CR>
nnoremap z. 10<C-w>>
nnoremap z, 10<C-w><
map = <C-w>=
nnoremap <F7> :mksession! ~/session.vim
nnoremap <F8> :source ~/session.vim <CR>
nnoremap <C-q> :wa <CR> :%bw <CR> :exit <CR>
nnoremap <F12> :call CocAction('format')<CR>


set splitright
"map <F1> :w <CR> :terminal c % <CR>

set guicursor=

"set colorcolumn=80
"highlight ColorColumn ctermbg=6

"remember cursor position
set viminfo='10,\"100,:20,%,n~/.viminfo

" show what command I'm typing
set showcmd

" sane indentation
" useful for exams
set tabstop=8
set shiftwidth=4
set softtabstop=4
set expandtab
set smarttab

""" AIRLINE """
"let g:airline_theme='solarized'
"
" enable/disable displaying buffers with a single tab
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#tab_nr_type = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline_powerline_fonts = 0
let g:airline#extensions#tabline#show_tab_nr = 0
let g:airline_left_sep = ''
let g:airline_right_sep = ''
let g:airline_section_b = '%{coc#status()}'
""" AIRLINE END """

"syntax enable
"set background=dark
"colorscheme codedark
"set t_co=256
""let g:molokai_original = 1
"let g:solarized_termcolors=256

"colorscheme molokai
highlight LineNr ctermfg=white ctermbg=darkgrey
"set colorscheme solarized


"set completeopt+=noselect
"set completeopt+=menuone
"let g:mucomplete#enable_auto_at_startup = 1
"set shortmess+=c   " Shut off completion messages 

" filetype plugin on
" set omnifunc=syntaxcomplete#Complete

"Open nerdtree with ctrl+n
map <C-n> :NERDTreeToggle<CR>

set nocompatible
filetype off
"set the runtime path to include Vundle and initialise
set rtp+=~/.vim/bundle/Vundle.vim

call plug#begin()
Plug 'VundleVim/Vundle.vim'
Plug 'scrooloose/nerdtree'
Plug 'neoclide/coc.nvim', {'branch' : 'release'}
Plug 'vim-airline/vim-airline'
call plug#end()
