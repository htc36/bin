#!/bin/bash

gcc -std=c99 -Werror -Wall -g -o ${1//.c} $1 -lm && ./${1//.c}

